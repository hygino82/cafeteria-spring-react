package com.cafeteria.codeblack.dto;

import java.math.BigDecimal;

import org.hibernate.validator.constraints.URL;

import com.cafeteria.codeblack.enums.CategoryEnum;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record ProductDTO(
		@NotEmpty String title, @NotEmpty String description, 
		@NotNull @URL String photo,
		@NotNull Integer size,
		@NotNull @DecimalMin(value = "0.0", inclusive = false) @Digits(integer = 3, fraction = 2) BigDecimal price,
		@NotNull Integer preparation, @NotNull CategoryEnum categoryId) {
}
