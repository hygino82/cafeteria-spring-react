package com.cafeteria.codeblack.utils;

import java.net.URI;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cafeteria.codeblack.dto.ProductDTO;
import com.cafeteria.codeblack.entities.Product;

public interface GeneralUtitilies {

	public static URI toUri(String path) {
		return URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toString());
	}

	public static Product productDtoToEntity(Product entity, ProductDTO dto) {
		entity.setTitle(dto.title());
		entity.setDescription(dto.description());
		entity.setPhoto(dto.photo());
		entity.setSize(dto.size());
		entity.setPrice(dto.price());
		entity.setPreparation(dto.preparation());
		entity.setCategoryId(dto.categoryId());
		
		return entity;
	}
}
