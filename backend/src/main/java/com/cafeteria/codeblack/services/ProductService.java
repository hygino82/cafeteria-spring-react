package com.cafeteria.codeblack.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cafeteria.codeblack.dto.ProductDTO;
import com.cafeteria.codeblack.entities.Product;

import jakarta.validation.Valid;

public interface ProductService {

	Page<Product> getProducts(Pageable pageable);

	Product saveProduct(ProductDTO productDTO);

	Product getProduct(Long productId);

	Product update(@Valid ProductDTO dto, Long productId);

	void remove(Long productId);

	Product findProductById(Long productId);

	Product findProductByTitle(String title);
}
