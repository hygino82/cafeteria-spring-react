package com.cafeteria.codeblack.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cafeteria.codeblack.dto.ProductDTO;
import com.cafeteria.codeblack.entities.Product;
import com.cafeteria.codeblack.repositories.ProductRepository;
import com.cafeteria.codeblack.utils.GeneralUtitilies;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;

	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public Page<Product> getProducts(Pageable pageable) {
		return productRepository.findAll(pageable);
	}

	@Override
	public Product saveProduct(ProductDTO productDTO) {
		Product entity = new Product(productDTO);
		// Product entity = mapper.map(productDTO, Product.class);
		entity = productRepository.save(entity);
		return entity;
	}

	@Override
	public Product getProduct(Long productId) {
		Product product = productRepository.findById(productId).get();
		return product;
	}

	@Override
	public Product findProductById(Long productId) {
		return productRepository.findByProductId(productId);
	}

	@Override
	public Product findProductByTitle(String title) {
		Product data = productRepository.findByTitle(title);
		return data;
	}

	@Override
	public Product update(ProductDTO productDTO, Long productId) {
		Product entity = productRepository.findByProductId(productId);
		entity = GeneralUtitilies.productDtoToEntity(entity, productDTO);
		return productRepository.save(entity);
	}

	@Override
	public void remove(Long productId) {
		Product entity = productRepository.findByProductId(productId);
		productRepository.delete(entity);
	}
}