package com.cafeteria.codeblack.enums;

public enum CategoryEnum {
	BEBIDAS_QUENTES, 
	BEBIDAS_FRIAS, 
	COMIDAS, 
	ACESSORIOS
}
