package com.cafeteria.codeblack.controllers;

import java.net.URI;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cafeteria.codeblack.dto.ProductDTO;
import com.cafeteria.codeblack.entities.Product;
import com.cafeteria.codeblack.services.ProductService;
import com.cafeteria.codeblack.utils.GeneralUtitilies;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/product")
@CrossOrigin("*")
public class ProductController {

	private final ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	@GetMapping
	@Tag(name = "Listar todos os produtos")
	public ResponseEntity<Page<Product>> getProducts(Pageable pageable) {
		Page<Product> page = productService.getProducts(pageable);
		return ResponseEntity.status(200).body(page);
	}

	@GetMapping("/{productId}")
	@Tag(name = "Buscar produto por Id")
	public ResponseEntity<Product> findProductById(@PathVariable("productId") Long productId) {
		Product data = productService.findProductById(productId);
		return ResponseEntity.status(200).body(data);
	}

	@GetMapping("show/{title}")
	@Tag(name = "Buscar produto por Título")
	public ResponseEntity<Product> findProductByTitle(@PathVariable("title") String title) {
		Product data = productService.findProductByTitle(title);
		return ResponseEntity.status(200).body(data);
	}

	@PostMapping
	@Tag(name = "Inserir um novo produto")
	public ResponseEntity<Product> insert(@RequestBody @Valid ProductDTO dto) {
		URI uri = GeneralUtitilies.toUri("api/v1/product");
		Product data = productService.saveProduct(dto);
		return ResponseEntity.created(uri).body(data);
	}

	@DeleteMapping("/{productId}")
	@Tag(name = "Remover um produto pelo seu Id")
	public void remove(@PathVariable("productId") Long productId) {
		productService.remove(productId);
	}

	@PutMapping("/{productId}")
	@Tag(name = "Atualizar um produto pelo seu Id")
	public ResponseEntity<Product> update(
			@PathVariable("productId") Long productId,
			@RequestBody @Valid ProductDTO dto) {
		Product data = productService.update(dto, productId);
		return ResponseEntity.status(200).body(data);
	}
}
