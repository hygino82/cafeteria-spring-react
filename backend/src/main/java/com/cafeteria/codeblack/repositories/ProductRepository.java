package com.cafeteria.codeblack.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cafeteria.codeblack.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	Product findByTitle(String title);
	Optional<Product> findByCategoryId(Long categoryId);
	Product findByProductId(Long productId);
}
