package com.cafeteria.codeblack.entities;

import java.math.BigDecimal;

import com.cafeteria.codeblack.dto.ProductDTO;
import com.cafeteria.codeblack.enums.CategoryEnum;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long productId;

	private String title;
	private String description;
	private String photo;
	private Integer size;
	private BigDecimal price;
	private Integer preparation;
	private CategoryEnum categoryId;

	public Product(ProductDTO dto) {
		title = dto.title();
		description = dto.description();
		photo = dto.photo();
		size = dto.size();
		price = dto.price();
		preparation = dto.preparation();
		categoryId = dto.categoryId();
	}
}
